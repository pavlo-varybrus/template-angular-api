﻿/// <reference path="services/global-constant.ts" />
/// <reference path="services/app-http-service.ts" />

/// <reference path="services/authservice.ts" />
/// <reference path="services/auth-interceptor-service.ts" />

/// <reference path="states/about/about-controller.ts" />
/// <reference path="states/about/about-module.ts" />

/// <reference path="states/associate/associate-controller.ts" />
/// <reference path="states/associate/associate-module.ts" />


/// <reference path="states/login/login-controller.ts" />
/// <reference path="states/login/login-module.ts" />

/// <reference path="services/models/models.ts" />

/// <reference path="states/dashboard/edit-category.ts" />
/// <reference path="states/dashboard/dashboard-service.ts" />
/// <reference path="states/dashboard/dashboard-controller.ts" />
/// <reference path="states/dashboard/dashboard-module.ts" />

/// <reference path="states/header/header-controller.ts" />
/// <reference path="states/header/header-module.ts" />


/// <reference path="app-start.ts" />
