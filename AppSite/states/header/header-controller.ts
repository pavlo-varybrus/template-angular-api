﻿module Illuminati.Header {
    'use strict';
    export class HeaderController {
        private authService;
        private $location;
        private $scope;
        constructor(authService, $location, $scope) {
            this.authService = authService;
            this.$location = $location;
            this.$scope = $scope;

            $scope.brandName = GlobalConstant.Config.brandName;
            $scope.authentication = authService.authentication;
            $scope.logOut = this.logOut;
        }

        public logOut = () => {
            this.authService.logOut();
            this.$location.path(GlobalConstant.FormUrl.landingPage);
        }
    }
};