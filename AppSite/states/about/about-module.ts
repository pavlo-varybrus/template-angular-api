﻿module Illuminati.About {
    'use strict';
    export var aboutModule = angular.module('aboutModule', []);

    function configuration($stateProvider) {
        $stateProvider.state('about', {
            url: '/about',
            templateUrl: 'states/about/about.html',
            controller: 'aboutCtrl as about'
        });
    }

    aboutModule
        .config(configuration)
        .controller('aboutCtrl', About.AboutController);

}

