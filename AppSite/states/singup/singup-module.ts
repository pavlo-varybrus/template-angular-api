﻿module Illuminati.Singup {
    'use strict';
    export var singupModule = angular.module('singupModule', []);

    function configuration($stateProvider) {
        $stateProvider.state('singup', {
            url: '/singup',
            templateUrl: 'states/singup/singup.html',
            controller: 'singupCtrl as singup'
        });
    }

    singupModule
        .config(configuration)
        .controller('singupCtrl', Singup.SingupController);
}

