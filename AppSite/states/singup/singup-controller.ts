﻿module Illuminati.Singup {
    import AuthService = Illuminati.Service.AuthService;
    'use strict';
    export class SingupController {
        constructor($scope, $location, $timeout, authService: AuthService) {
            $scope.savedSuccessfully = false;
            $scope.message = "";

            $scope.registration = {
                userName: "",
                password: "",
                confirmPassword: ""
            };

            $scope.signUp = () => {
                authService.saveRegistration($scope.registration).then((response) => {
                    authService._login(false, response.data.access_token, response.data.userName, "");
                    $location.path(GlobalConstant.FormUrl.home);
                }, (response) => {
                    var errors = [];
                    for (var key in response.data.modelState) {
                        for (var i = 0; i < response.data.modelState[key].length; i++) {
                            errors.push(response.data.modelState[key][i]);
                        }
                    }
                    $scope.message = "Failed to register user due to:" + errors.join(' ');
                });
            };
        }

    }
};

