﻿module Illuminati.Dashboard {
    'use strict';

    export class DashboardService {
        private CategoryResorce;
        private StockResorce;
        private StockToCategoryResorce;
        private CategoryWithCountResorce;
        private StockResorceWithCategory;

        constructor($resource) {
            this.CategoryResorce = $resource(GlobalConstant.UrlConstant.category + '/:id', null, { 'update': { method: 'PUT' } });
            this.CategoryWithCountResorce = $resource(GlobalConstant.UrlConstant.category + '/:id/count', null, { 'update': { method: 'PUT' } });
            this.StockResorce = $resource(GlobalConstant.UrlConstant.stock + '/:id', null, { 'update': { method: 'PUT' } });
            this.StockResorceWithCategory = $resource(GlobalConstant.UrlConstant.stock + '/:id/category', null, { 'update': { method: 'PUT' } });
            this.StockToCategoryResorce = $resource(GlobalConstant.UrlConstant.stockToCategory + '/:id', null, { 'update': { method: 'PUT' } });
        }

        public initWatch(stockList: Array<Models.Stock>, mainCategoryList: Array<Models.Category>) {
            angular.forEach(stockList, (stock: Models.Stock) => {
                stock.categoriesList = [];
                angular.forEach(stock.categories, (stockCategory: Models.Category) => {
                    var category = $.grep(mainCategoryList, (mainCategory: Models.Category, index) => { return mainCategory.id === stockCategory.id });
                    stock.categoriesList.push(category[0]);
                });
            });
        }

        public getCategories = (success) => {
            this.CategoryWithCountResorce.query(categoryList => success(categoryList));
        }
        public deleteCategory = (category: Models.Category) => {
            var categoryResorce = new this.CategoryResorce();
            categoryResorce.$delete({ id: category.id });
        }

        public addCategory = (categoryName, success) => {
            var categoryResorce = new this.CategoryResorce();
            categoryResorce.CategoryName = categoryName;
            categoryResorce.color = "#ff6600";
            categoryResorce.$save(success);
        }

        public updateCategory = (category: Models.Category, success) => {
            this.CategoryResorce.update({ id: category.id }, category);
        }

        public getStock = (success) => {
            this.StockResorceWithCategory.query(list => success(list));
        }

        public deleteStockToCategory = (id) => {
            var stockToCategoryResorce = new this.StockToCategoryResorce();
            stockToCategoryResorce.$delete({ id: id });
        }

        public addStockToCategory = (stockId, categoryId, success) => {
            var stockToCategoryResorce = new this.StockToCategoryResorce();
            stockToCategoryResorce.categoryId = categoryId;
            stockToCategoryResorce.stockId = stockId;
            stockToCategoryResorce.$save(success);
        }

        getStockToCategory(stock: Models.Stock, categoryId: number) {
            var res = $.grep(stock.stockToCategoryList, (stc, t) => { return stc.categoryId === categoryId })[0];
            return res;
        }

    }
};