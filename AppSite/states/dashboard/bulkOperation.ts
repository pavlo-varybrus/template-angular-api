﻿module Illuminati.Dashboard {
    'use strict';

    export class BulkOperation {
        private categoryList;
        private cbAttachCategory;
        private cbdeleteBulkCategory;
        private selectedCategory = [];
        private isShow = false;

        constructor(categoryList, cbAttachCategory, cbdeleteBulkCategory) {
            this.categoryList = categoryList;
            this.cbAttachCategory = cbAttachCategory;
            this.cbdeleteBulkCategory = cbdeleteBulkCategory;
        }

        public toggleSelection = (category) => {
            var idx = this.selectedCategory.indexOf(category);
            // is currently selected
            if (idx > -1) {
                this.selectedCategory.splice(idx, 1);
                if (this.cbdeleteBulkCategory)
                    this.cbdeleteBulkCategory(category);
            }
            // is newly selected
            else {
                this.selectedCategory.push(category);
                if (this.cbAttachCategory)
                    this.cbAttachCategory(category);
            }
        }
        public popupClick = (entity, $event) => {
            this.isShow = true;
            //this.entity = entity;
            //this.cell = $($event.target.parentElement);
            this.setPopupPosition();
        }

        public closeClicked = () => {
            this.isShow = false;
        }

        private setPopupPosition = () => {
            //if (!this.isShow)
            //    return;
            //setTimeout(() => {
            //    var offest = this.cell.offset();
            //    var w = $(window);
            //    $(this.selectorTemplate).css({ top: offest.top - w.scrollTop() + 30, left: offest.left - w.scrollLeft() });
            //}, 50);
        }
    }
}