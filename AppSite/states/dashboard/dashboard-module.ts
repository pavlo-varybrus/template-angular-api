﻿module Illuminati.Dashboard {
    'use strict';

    export var dashboardModule = angular.module('dashboardModule', []);

    function configuration($stateProvider) {
        $stateProvider.state('dashboard', {
            url: '/dashboard',
            templateUrl: 'states/dashboard/dashboard.html',
            controller: 'dashboardCtrl as dashboard'
        });
    }

    dashboardModule
        .config(configuration)
        .service('dashboardService', Dashboard.DashboardService)
        .controller('dashboardCtrl', Dashboard.DashboardController);

}

