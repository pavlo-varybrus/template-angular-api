﻿module Illuminati.Dashboard {
    'use strict';

    export class DashboardController {
        private uiGridConstants;
        public categoryList;
        public popupStockCategories;
        public popup;
        public editCategory: EditCategory;
        public bulkOperation;
        private gridWatchListOptions;
        private $scope;
        private $templateCache;
        private visibleShortCategory = 4;

        private dashboardService: DashboardService;

        constructor(dashboardService: DashboardService, $scope, $templateCache, uiGridConstants) {
            this.uiGridConstants = uiGridConstants;
            this.uiGridConstants.scrollbars.WHEN_NEEDED = 2;
            this.popup = new PopupElement("#popoverTemplate", this.detachCategory);
            this.$templateCache = $templateCache;
            this.$scope = $scope;
            this.dashboardService = dashboardService;

            this.initGrid();

            dashboardService.getCategories((categoryList) => {
                dashboardService.getStock((watchList) => {
                    dashboardService.initWatch(watchList, <any>categoryList);
                    //
                    this.bulkOperation = new BulkOperation(categoryList, this.attacheBulkCategory, this.deleteBulkCategory);
                    this.categoryList = categoryList;
                    var categoryByDefault = categoryList[0];
                    this.editCategory = new EditCategory(categoryByDefault, this.deleteCategory, this.updateCategory, this.$scope);
                    //
                    this.gridWatchListOptions.data = watchList;
                });
            });
        }

        public updateCategory = (category: Models.Category) => {
            this.dashboardService.updateCategory(category, (category) => { });
        }

        public deleteCategory = (category: Models.Category) => {
            this.dashboardService.deleteCategory(category);
            var indx = this.categoryList.indexOf(category);
            this.categoryList.splice(indx, 1);
            angular.forEach(this.gridWatchListOptions.data, (stock: Models.Stock) => {
                this.detachCategory(stock, category);
            });

        }

        public addCategory = (categoryName: Models.Category) => {
            this.dashboardService.addCategory(categoryName, (category) => {
                this.categoryList.push(category);
            });
        }

        private initGrid = () => {
            var self = this;
            var template = this.$templateCache.get('grid-category-cell.html').trim();
            this.gridWatchListOptions = {
                enableHorizontalScrollbar: this.uiGridConstants.scrollbars.WHEN_NEEDED,
                enableRowSelection: true,
                multiSelect: true,
                rowHeight: 32,
                onRegisterApi: function (gridApi) {
                    self.$scope.gridApi = gridApi;
                    gridApi.selection.on.rowSelectionChanged(self.$scope, row => {
                        self.gridWatchListOptions.selectedRow = row;
                    });
                },
                columnDefs: [
                    { name: 'id', field: 'id' },
                    { name: 'category', field: 'categoriesList', display: "sss", cellTemplate: template, width: 200 }
                ]
            };
        }

        public attacheBulkCategory = (category: Models.Category) => {
            if (this.$scope.gridApi.selection.getSelectedRows().length > 0) {
                angular.forEach(this.$scope.gridApi.selection.getSelectedRows(), (stock, key) => {
                    this.attacheCategory(stock, category);
                });
            }
            else if (this.$scope.gridApi.cellNav.getFocusedCell()) {
                var stock = this.$scope.gridApi.cellNav.getFocusedCell().row.entity;
                this.attacheCategory(stock, category);
            }
            else {
                alert("this.gridWatchListOptions.selectedRow");
            }
        }

        public deleteBulkCategory = (category) => {
            if (this.$scope.gridApi.selection.getSelectedRows().length > 0) {
                angular.forEach(this.$scope.gridApi.selection.getSelectedRows(), (stock, key) => {
                    this.detachCategory(stock, category);
                });
            }
        }

        public attacheCategory(stock: Models.Stock, category: Models.Category) {
            //prevent add the same category
            var elementAlreadyAdded = $.grep(stock.categoriesList, (categorItem: any, key) => {
                return categorItem.id === category.id;
            }).length > 0;
            if (elementAlreadyAdded)
                return;
            stock.categoriesList.push(category);
            this.dashboardService.addStockToCategory(stock.id, category.id, (stockToCategory) => {
                stock.stockToCategoryList.push(stockToCategory);
            });


            this.callbackChangeCategory(category, 1);
        }

        public callbackChangeCategory = (category, operation) => {
            this.refreshCategoryCount(category, operation);
        }

        private refreshCategoryCount = (category, operation) => {
            angular.forEach(this.categoryList, (categoryItem, key) => {
                if (categoryItem.id === category.id) {
                    categoryItem.count += operation;
                }
            });
        }

        private categoryClicked = (category: Models.Category) => {
            this.editCategory.changeCategory(category);
        }

        public detachCategory = (stock: Models.Stock, category: Models.Category) => {
            angular.forEach(stock.categoriesList, (item, index) => {
                if (item.id === category.id) {
                    stock.categoriesList.splice(index, 1);
                    this.dashboardService.deleteStockToCategory(this.dashboardService.getStockToCategory(stock, category.id).id);
                    var indStockToCategoryList = stock.stockToCategoryList.indexOf(this.dashboardService.getStockToCategory(stock, category.id));
                    stock.stockToCategoryList.splice(indStockToCategoryList, 1);
                    if (this.callbackChangeCategory) this.callbackChangeCategory(category, -1);
                }
            });
        }
    }


};