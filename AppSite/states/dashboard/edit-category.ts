﻿module Illuminati.Dashboard {
    'use strict';

    export class EditCategory {
        private category: Models.Category;
        private categoryByDefault;
        private saveCategoryName;
        private isEditState = false;
        private canEdit;
        private cbDeleteCategory;
        private cbUpdateCategory;

        constructor(categoryByDefault, cbDeleteCategory, cbUpdateCategory, $scope) {
            this.categoryByDefault = categoryByDefault;
            this.category = categoryByDefault;
            this.cbDeleteCategory = cbDeleteCategory;
            this.cbUpdateCategory = cbUpdateCategory;
        }

        public changeCategory = (category: Models.Category) => {
            this.category = category;
            this.isEditState = false;
            this.canEdit = category.categoryType !== 'System';
        }

        private renameClicked = () => {
            this.isEditState = true;
            this.saveCategoryName = this.category.categoryName;
        }

        private deleteClicked = (confirm) => {
            this.cbDeleteCategory(this.category);
            this.category = this.categoryByDefault;
        }

        private changeText = () => {
            this.cbUpdateCategory(this.category);
            this.isEditState = false;
        }

        private changeColor = (val) => {
            this.category.color = val;
            this.cbUpdateCategory(this.category);
        }

        private declainChangeText = () => {
            this.category.categoryName = this.saveCategoryName;
            this.isEditState = false;
        }
    }
}