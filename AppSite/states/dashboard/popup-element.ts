﻿module Illuminati.Dashboard {
    'use strict';

    export class PopupElement {
        private isShow = false;
        private entity = null;
        private cell = null;
        private selectorTemplate = null;
        private callbackdetachCategory = null;

        constructor(selectorTemplate, calbackdetachCategory) {
            this.selectorTemplate = selectorTemplate;
            this.callbackdetachCategory = calbackdetachCategory;
            $(window).resize(() => { this.setPopupPosition(); });
        }

        public detachCategory = (entity, categoryItem) => {
            if (this.callbackdetachCategory) this.callbackdetachCategory(entity, categoryItem);
        }

        public popupClick = (entity, $event) => {
            this.isShow = true;
            this.entity = entity;
            this.cell = $($event.target.parentElement);
            this.setPopupPosition();
        }

        public closeClicked = () => {
            this.isShow = false;
        }

        private setPopupPosition = () => {
            if (!this.isShow)
                return;
            setTimeout(() => {
                var offest = this.cell.offset();
                var w = $(window);
                $(this.selectorTemplate).css({ top: offest.top - w.scrollTop() + 30, left: offest.left - w.scrollLeft() });
            }, 50);
        }
    }
}