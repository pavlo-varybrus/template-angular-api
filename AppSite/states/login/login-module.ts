﻿module Illuminati.Login {
    'use strict';
    export var loginModule = angular.module('loginModule', []);

    function configuration($stateProvider) {
        $stateProvider.state('login', {
            url: '/login',
            templateUrl: 'states/login/login.html',
            controller: 'loginCtrl as login'
        });
    }

    loginModule
        .config(configuration)
        .controller('loginCtrl', Login.LoginController);

}

