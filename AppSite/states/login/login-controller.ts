﻿module Illuminati.Login {
    'use strict';
    declare var window;
    export class LoginController {
        private authService;
        private $location;
        private $scope;
        public loginData = {
            userName: "",
            password: "",
            useRefreshTokens: false
        };
        public message = "";

        constructor($scope, $location, authService) {
            this.authService = authService;
            this.$location = $location;
            this.$scope = $scope;
            this.$scope.loginData = this.loginData;
            this.$scope.message = this.message;
            this.$scope.authExternalProvider = this.authExternalProvider;
            this.$scope.authCompletedCB = this.authCompletedCB;
        }

        login = () => {
            this.authService.login(this.loginData).then((response) => {
                this.$location.path(GlobalConstant.FormUrl.home);
            },
                (err) => {
                    this.message = err.error_description;
                });
        };

        authExternalProvider = (provider) => {
            var redirectUri = location.protocol + '//' + location.host + '/authcomplete.html';
            var externalProviderUrl = GlobalConstant.UrlConstant.account.externalLogin + "?provider=" + provider
                + "&response_type=token&client_id=" + GlobalConstant.Config.clientId
                + "&redirect_uri=" + redirectUri;
            window.$windowScope = this.$scope;
            var oauthWindow = window.open(externalProviderUrl, "Authenticate Account", "location=0,status=0,width=600,height=750");
        };
        authCompletedCB = (fragment) => {
            if (fragment.haslocalaccount === 'False') {
                this.authService.logOut();
                this.authService.externalAuthData = {
                    provider: fragment.provider,
                    userName: fragment.external_user_name,
                    externalAccessToken: fragment.external_access_token
                };

                this.authService.registerExternal(this.authService.externalAuthData).then((response) => {
                    this.$scope.savedSuccessfully = true;
                    this.$scope.message = "User has been registered successfully, you will be redicted to orders page in 2 seconds.";
                    this.$location.path(GlobalConstant.FormUrl.home);
                },
                    (response) => {
                        var errors = [];
                        for (var key in response.modelState) {
                            errors.push(response.modelState[key]);
                        }
                        this.$scope.message = "Failed to register user due to:" + errors.join(' ');
                    });
            } else {
                //Obtain access token and redirect to orders
                var externalData = { provider: fragment.provider, externalAccessToken: fragment.external_access_token };
                this.authService.obtainAccessToken(externalData).then((response) => {
                    this.$location.path(GlobalConstant.FormUrl.home);

                },
                    (err) => {
                        this.$scope.message = err.error_description;
                    });
            }
            ;
        }
        //authCompletedCB = (fragment) => {
        //    this.$scope.$apply(() => {
        //        if (fragment.haslocalaccount === 'False') {
        //            this.authService.logOut();
        //            this.authService.externalAuthData = {
        //                provider: fragment.provider,
        //                userName: fragment.external_user_name,
        //                externalAccessToken: fragment.external_access_token
        //            };
        //            this.$location.path(GlobalConstant.FormUrl.associate);
        //        }
        //        else {
        //            //Obtain access token and redirect to orders
        //            var externalData = { provider: fragment.provider, externalAccessToken: fragment.external_access_token };
        //            this.authService.obtainAccessToken(externalData).then((response) => {
        //                this.$location.path(GlobalConstant.FormUrl.home);

        //            },
        //                (err) => {
        //                    this.$scope.message = err.error_description;
        //                });
        //        }

        //    });
        //}
    }



};