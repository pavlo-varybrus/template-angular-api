﻿module Illuminati.Associate {
    'use strict';
    export var associateModule = angular.module('associateModule', []);

    function configuration($stateProvider) {
        $stateProvider.state('associate', {
            url: '/associate',
            templateUrl: 'states/associate/associate.html',
            controller: 'associateCtrl as associate'
        });
    }

    associateModule
        .config(configuration)
        .controller('associateCtrl', Associate.AssociateController);

}

