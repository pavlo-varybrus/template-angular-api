﻿module Illuminati.Service {
    'use strict';
    export class AuthInterceptorService {
        public $q;
        public $location;
        public localStorageService;
        public constructor($q, $location, localStorageService) {
            this.localStorageService = localStorageService;
            this.$location = $location;
            this.$q = $q;
        }

        request = (config) => {
            config.headers = config.headers || {};
            var authData = this.localStorageService.get('authorizationData');
            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }
            return config;
        }

        responseError = (rejection) => {
            if (rejection.status === 401) {
                this.$location.path(GlobalConstant.FormUrl.login);
            }
            return this.$q.reject(rejection);
        }
    }
}

