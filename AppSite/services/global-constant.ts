﻿module Illuminati.GlobalConstant {

    export class FormUrl {
        public static get login(): string { return "/login" }
        public static get about(): string { return "/about" }
        public static get home(): string { return "/dashboard" }
        public static get landingPage(): string { return "/about" }
        public static get associate(): string { return "/associate" }
    }

    export class Config {
        public static get apiServiceBaseUri(): string { return "http://localhost:43808/api" }
        public static get clientId(): string { return 'ngAuthApp' }
        public static get brandName(): string { return "Stock Watcher" }
        public static get Url(): string { return "Stock Watcher" }
    }

    export class UrlConstant {

        public static get category(): string { return Config.apiServiceBaseUri + "/category" }
        public static get account(): any {
            return {
                "register": Config.apiServiceBaseUri + "/account/register",
                "obtainLocalAccessToken": Config.apiServiceBaseUri + "/account/obtainLocalAccessToken",
                "registerexternal": Config.apiServiceBaseUri + "/account/registerexternal",
                "externalLogin": Config.apiServiceBaseUri + "/account/externallogin",
                "token": Config.apiServiceBaseUri + "/token"
            }
        }
        public static get stock(): string { return Config.apiServiceBaseUri + "/stock" }
        public static get stockToCategory(): string { return Config.apiServiceBaseUri + "/stockToCategory" }
    }
}



