﻿module Illuminati.Service {
    'use strict';
    declare var window: any;
    export class HttpServiceOptions {
        constructor(url, model = {}, cbS: any = null, cbE: any = null) {
            this.cbE = cbE;
            this.cbS = cbS;
            this.model = model;
            this.url = url;
        }

        url: string;
        model: Object;
        cbS;
        cbE;
    }
    export class HttpService {
        private $state;
        private $window;
        constructor($http, $state, loadingService) {
            this.$state = $state;
            var self = this;
            self.loadingService = loadingService;
            self.$http = $http;
        }
        PostJsonWithLoading = (options: HttpServiceOptions) => {
            this.CommonPostJson(options, true);
        };
        PostJson = (options: HttpServiceOptions) => {
            this.CommonPostJson(options, false);
        };

        CommonPostJson = (options: HttpServiceOptions, isShowLoading) => {
            var self = this;
            isShowLoading = false;
            if (isShowLoading) {
                self.loadingService.Show();
            }
            //
            if (options.url.indexOf('?') > 0) {
                options.url += "&";

            } else {
                options.url += "?";
            }
            window.userService.retrieveSavedData();
            options.url += "token=" + window.userService.userData.guidIdToken;
            //
            return this.$http({
                method: 'POST',
                url: options.url,
                cache: false,
                data: options.model,
                headers: { 'Content-Type': 'application/json' }
            }).success(function (data, status) {
                if (options.cbS) {
                    options.cbS(data, status);
                }
                if (isShowLoading) {
                    self.loadingService.Hide();
                }

            }).error((data, status) => {
                if (options.cbE) {
                    options.cbE(data, status);
                } else {
                    if (status === 401) {
                        window.userService.removeAuthentication();
                        self.$state.go('login', {}, { reload: true });
                    } else {
                        debugger;
                        //alert("Errord url=" + options.url);
                        console.log(data);
                    }

                }
                if (isShowLoading) {
                    self.loadingService.Hide();
                }
            });
        };
        loadingService;
        $http;
    }
}