﻿module Illuminati.Service {
    "use strict";

    export class AuthService {
        private $http;
        private $q;
        private localStorageService;

        public constructor($http, $q, localStorageService) {
            this.$http = $http;
            this.$q = $q;
            this.localStorageService = localStorageService;
        }

        authentication = {
            isAuth: false,
            userName: "",
            useRefreshTokens: false
        };

        externalAuthData = {
            provider: "",
            userName: "",
            externalAccessToken: ""
        };
        saveRegistration = (registration) => {
            this.logOut();

            return this.$http.post(GlobalConstant.UrlConstant.account.register, registration).then(function (response) {
                return response;
            });

        };

        _login = (useRefreshTokens, accessToken, userName, refreshToken) => {
            if (useRefreshTokens) {
                this.localStorageService.set("authorizationData", { token: accessToken, userName: userName, refreshToken: refreshToken, useRefreshTokens: true });
            } else {
                this.localStorageService.set("authorizationData", { token: accessToken, userName: userName, refreshToken: "", useRefreshTokens: false });
            }
            this.authentication.isAuth = true;
            this.authentication.userName = userName;
            this.authentication.useRefreshTokens = useRefreshTokens;
        }

        login = (loginData) => {
            var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;
            if (loginData.useRefreshTokens) {
                data = data + "&client_id=" + GlobalConstant.Config.clientId;
            }
            var deferred = this.$q.defer();
            this.$http.post(GlobalConstant.UrlConstant.account.token, data, { headers: { 'Content-Type': "application/x-www-form-urlencoded" } }).success((response) => {
                this._login(loginData.useRefreshTokens, response.access_token, loginData.userName, response.refresh_token);
                //if (loginData.useRefreshTokens) {
                //    this.localStorageService.set("authorizationData", { token: response.access_token, userName: loginData.userName, refreshToken: response.refresh_token, useRefreshTokens: true });
                //} else {
                //    this.localStorageService.set("authorizationData", { token: response.access_token, userName: loginData.userName, refreshToken: "", useRefreshTokens: false });
                //}
                //this.authentication.isAuth = true;
                //this.authentication.userName = loginData.userName;
                //this.authentication.useRefreshTokens = loginData.useRefreshTokens;
                deferred.resolve(response);

            }).error((err, status) => {
                this.logOut();
                deferred.reject(err);
            });
            return deferred.promise;
        };

        logOut = function () {
            this.localStorageService.remove("authorizationData");
            this.authentication.isAuth = false;
            this.authentication.userName = "";
            this.authentication.useRefreshTokens = false;
        };

        private fillAuthData = function () {
            var authData = this.localStorageService.get("authorizationData");
            if (authData) {
                this.authentication.isAuth = true;
                this.authentication.userName = authData.userName;
                this.authentication.useRefreshTokens = authData.useRefreshTokens;
            }

        };

        refreshToken = () => {
            var deferred = this.$q.defer();
            var authData = this.localStorageService.get("authorizationData");
            if (authData) {
                if (authData.useRefreshTokens) {
                    var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=" + GlobalConstant.Config.clientId;
                    this.localStorageService.remove("authorizationData");
                    this.$http.post(GlobalConstant.UrlConstant.account.token, data, { headers: { 'Content-Type': "application/x-www-form-urlencoded" } }).success((response) => {
                        this.localStorageService.set("authorizationData", { token: response.access_token, userName: response.userName, refreshToken: response.refresh_token, useRefreshTokens: true });
                        deferred.resolve(response);
                    }).error((err, status) => {
                        this.logOut();
                        deferred.reject(err);
                    });
                }
            }
            return deferred.promise;
        };
        obtainAccessToken = (externalData) => {
            var deferred = this.$q.defer();
            this.$http.get(GlobalConstant.UrlConstant.account.obtainLocalAccessToken, { params: { provider: externalData.provider, externalAccessToken: externalData.externalAccessToken } })
                .success((response) => {
                    this.localStorageService.set("authorizationData", { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });
                    this.authentication.isAuth = true;
                    this.authentication.userName = response.userName;
                    this.authentication.useRefreshTokens = false;
                    deferred.resolve(response);
                }).error((err, status) => {
                    this.logOut();
                    deferred.reject(err);
                });
            return deferred.promise;
        };

        registerExternal = (registerExternalData) => {
            var deferred = this.$q.defer();
            this.$http.post(GlobalConstant.UrlConstant.account.registerexternal, registerExternalData)
                .success((response) => {
                    this.localStorageService.set("authorizationData", { token: response.access_token, userName: response.userName, refreshToken: "", useRefreshTokens: false });
                    this.authentication.isAuth = true;
                    this.authentication.userName = response.userName;
                    this.authentication.useRefreshTokens = false;
                    deferred.resolve(response);
                }).error((err, status) => {
                    this.logOut();
                    deferred.reject(err);
                });
            return deferred.promise;
        };
    }
}

