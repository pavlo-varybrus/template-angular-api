﻿module Illuminati.Models {
    export class Category {
        id: number;
        count: number;
        categoryName: string;
        categoryType: string;
        color: string;
        //shortName: string;
    }

    export class Stock {
        id: number;
        categoriesList: Array<Category>;///////////////del
        categories: Array<Category>;
        //categoryIdList: Array<number>[];
        stockToCategoryList: Array<StockToCategory>;
    }

    export class StockToCategory {
        categoryId: number;
        stockId: number;
        id: number;
    }
}