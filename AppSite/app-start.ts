﻿module Illuminati {
    "use strict";

    var app = angular.module("app", [
        'LocalStorageModule',
        'ngResource',
        'ui.bootstrap',
        'mwl.confirm',
        "pickAColor",
        "ui.router",
        "ngRoute",
        "ui.grid.edit", "ui.grid.rowEdit", "ui.grid.cellNav", "ui.grid.selection", "ui.grid.autoResize", "ui.grid",
        "singupModule",
        "loginModule",
        "dashboardModule",
        "aboutModule",
        "associateModule",
        "headerModule"
    ]);

    app.service("authService", Service.AuthService);
    app.service("authInterceptorService", Service.AuthInterceptorService);

    app.config(config);

    app.run(function (authService, $rootScope, $state) {
        authService.fillAuthData();
        $rootScope.IndexPage = {
            Title: GlobalConstant.Config.brandName,
            Description: ""
        }
    });

    var fixAngularIeCachingIssue = ($httpProvider) => {
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }
        // Answer edited to include suggestions from comments
        // because previous version of code introduced browser-related errors
        //disable IE ajax request caching
        $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
        // extra
        $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
        $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    }

    function config($httpProvider, $stateProvider, $urlRouterProvider) {
        fixAngularIeCachingIssue($httpProvider);
        $httpProvider.interceptors.push('authInterceptorService');
        $urlRouterProvider.otherwise("/dashboard");

    };
}
