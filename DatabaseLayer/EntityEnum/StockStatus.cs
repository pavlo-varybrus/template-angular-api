namespace DatabaseLayer.EntityEnum
{
    public enum StockStatus : short
    {
        None = 0,
        Open = 1,
        Close = 2
    }
}