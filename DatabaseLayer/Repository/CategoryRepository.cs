﻿//using System;
//using System.Collections.Generic;
//using System.Data.Entity;
//using System.Data.SqlClient;
//using System.Linq;
//using System.Linq.Expressions;
//using AutoMapper;
//using DatabaseLayer.Dto;


//namespace DatabaseLayer.Repository
//{
//    public partial class CategoryRepository
//    {
//        public IEnumerable<CategoryDto> GetCategoryWithCount()
//        {
//            string sql = @"
//select  c.[Id]
//      ,c.[CategoryName]
//      ,c.[CategoryType]
//      ,c.[Color]
//      ,c.[LastUpdate]
//      ,c.[ByUser_Id],
//	   count(*) as 'Count'
//from dbo.Categories c inner
//join dbo.StockCategories sc on c.Id = sc.Category_Id
//group by c.[Id]
//      ,c.[CategoryName]
//      ,c.[CategoryType]
//      ,c.[Color]
//      ,c.[LastUpdate]
//      ,c.[ByUser_Id]";
//            return context.Database.SqlQuery<CategoryDto>(sql).ToList();
//        }
//    }
//}