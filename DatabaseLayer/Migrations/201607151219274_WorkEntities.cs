namespace DatabaseLayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class WorkEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CategoryName = c.String(),
                        CategoryType = c.String(),
                        Color = c.String(),
                        LastUpdate = c.DateTime(nullable: false),
                        ByUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ByUser_Id)
                .Index(t => t.ByUser_Id);
            
            CreateTable(
                "dbo.Stocks",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Value = c.Int(nullable: false),
                        Status = c.Short(nullable: false),
                        LastUpdate = c.DateTime(nullable: false),
                        ByUser_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.ByUser_Id)
                .Index(t => t.ByUser_Id);
            
            CreateTable(
                "dbo.StockCategories",
                c => new
                    {
                        Stock_Id = c.Int(nullable: false),
                        Category_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Stock_Id, t.Category_Id })
                .ForeignKey("dbo.Stocks", t => t.Stock_Id, cascadeDelete: true)
                .ForeignKey("dbo.Categories", t => t.Category_Id, cascadeDelete: true)
                .Index(t => t.Stock_Id)
                .Index(t => t.Category_Id);
            
            AddColumn("dbo.Clients", "ApplicationType", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StockCategories", "Category_Id", "dbo.Categories");
            DropForeignKey("dbo.StockCategories", "Stock_Id", "dbo.Stocks");
            DropForeignKey("dbo.Stocks", "ByUser_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.Categories", "ByUser_Id", "dbo.AspNetUsers");
            DropIndex("dbo.StockCategories", new[] { "Category_Id" });
            DropIndex("dbo.StockCategories", new[] { "Stock_Id" });
            DropIndex("dbo.Stocks", new[] { "ByUser_Id" });
            DropIndex("dbo.Categories", new[] { "ByUser_Id" });
            DropColumn("dbo.Clients", "ApplicationType");
            DropTable("dbo.StockCategories");
            DropTable("dbo.Stocks");
            DropTable("dbo.Categories");
        }
    }
}
