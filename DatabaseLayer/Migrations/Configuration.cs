using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using DatabaseLayer.Entities;
using DatabaseLayer.EntityEnum;
using GlobalLayer;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DatabaseLayer.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<MainContext>
    {
        private const string SystemUserName = "System";
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(MainContext context)
        {
            //if (System.Diagnostics.Debugger.IsAttached == false)
            //{
            //    System.Diagnostics.Debugger.Launch();
            //}

            BuildUsersList(context);
            BuildClientsList(context);
            ApplicationUser systemUser = context.Users.First(u => u.UserName == SystemUserName);
            BuildCategoriesList(systemUser, context);
            context.SaveChanges();//get Categories id
            BuildStocksList(systemUser, context);
            context.SaveChanges();
        }

        private static void BuildClientsList(MainContext context)
        {
            if (!(context.Clients.Any()))
            {
                var clientList = new Client[]
                {
                    new Client
                    {
                        Id = "ngAuthApp",
                        Secret = Helper.GetHash("abc@123"),
                        Name = "AngularJS front-end Application",
                        ApplicationType = ApplicationTypes.JavaScript,
                        Active = true,
                        RefreshTokenLifeTime = 7200,
                        AllowedOrigin = GlobalSetting.Setting.Client.AuthAppAllowedOrigin
                    }
                };
                context.Clients.AddOrUpdate(clientList);
            }
        }

        private static void BuildUsersList(MainContext context)
        {
            if (!(context.Users.Any(u => u.UserName == SystemUserName)))
            {
                var userStore = new UserStore<ApplicationUser>(context);
                var userManager = new UserManager<ApplicationUser>(userStore);
                var userToInsert = new ApplicationUser { UserName = SystemUserName };
                userManager.Create(userToInsert, "PAssw1ord");
            }
        }

        private static void BuildCategoriesList(ApplicationUser systemUser, MainContext context)
        {
            if (!(context.Categories.Any()))
            {
                var list = new Category[]
                {
                    new Category()
                    {
                        CategoryName = "System",
                        CategoryType = "System",
                        Color = "#03A9F4",
                        ByUser = systemUser,
                        LastUpdate = DateTime.UtcNow
                    },
                    new Category()
                    {
                        CategoryName = "Public",
                        CategoryType = "my",
                        Color = "#33cc33",
                        ByUser = systemUser,
                        LastUpdate = DateTime.UtcNow
                    },
                    new Category()
                    {
                        CategoryName = "Private",
                        CategoryType = "my",
                        Color = "#ff0000",
                        ByUser = systemUser,
                        LastUpdate = DateTime.UtcNow
                    },
                    new Category()
                    {
                        CategoryName = "Gas",
                        CategoryType = "my",
                        Color = "#FFEB3B",
                        ByUser = systemUser,
                        LastUpdate = DateTime.UtcNow
                    },
                    new Category()
                    {
                        CategoryName = "Pride",
                        CategoryType = "my",
                        Color = "#E91E63",
                        ByUser = systemUser,
                        LastUpdate = DateTime.UtcNow
                    }
                };
                context.Categories.AddOrUpdate(list);
            }
        }

        private static void BuildStocksList(ApplicationUser systemUser, MainContext context)
        {
            if (!(context.Stocks.Any()))
            {
                var list = new Stock[]
                {
                    new Stock()
                    {
                        ByUser = systemUser,
                        LastUpdate = DateTime.UtcNow,
                        Value = 100,
                        Status = StockStatus.Close,
                        Categories = context.Categories.Where(
                            x => x.CategoryName == "System" || x.CategoryName == "Public" || x.CategoryName == "Private")
                            .ToList()
                    },
                    new Stock()
                    {
                        ByUser = systemUser,
                        LastUpdate = DateTime.UtcNow,
                        Value = 200,
                        Status = StockStatus.Open,
                        Categories =
                            context.Categories.Where(x => x.CategoryName == "System" || x.CategoryName == "Public")
                                .ToList()
                    },
                    new Stock()
                    {
                        ByUser = systemUser,
                        LastUpdate = DateTime.UtcNow,
                        Value = 300,
                        Status = StockStatus.None,
                        Categories = context.Categories.Where(x => x.CategoryName == "System").ToList()
                    },
                };
                context.Stocks.AddOrUpdate(list);
            }
        }

    }
}
