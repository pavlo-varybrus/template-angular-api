using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DatabaseLayer.Entities
{
    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string CategoryType { get; set; }
        public string Color { get; set; }
        public virtual ICollection<Stock> Stocks { get; set; }
        public virtual ApplicationUser ByUser { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}