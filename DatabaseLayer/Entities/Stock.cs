using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DatabaseLayer.EntityEnum;

namespace DatabaseLayer.Entities
{
    public class Stock
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public ICollection<Category> Categories { get; set; }
        public int Value { get; set; }
        public virtual StockStatus Status { get; set; }
        public virtual ApplicationUser ByUser { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}

