﻿using System.Data.Entity;
using DatabaseLayer.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DatabaseLayer
{

    public class MainContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Client> Clients { get; set; }
        public DbSet<RefreshToken> RefreshTokens { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Stock> Stocks { get; set; }
        public MainContext() : base("MainContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }
    }
}