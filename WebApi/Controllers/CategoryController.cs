﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using BusinessLayer;
using DatabaseLayer.Entities;

using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    public class CategoryController : ApiController
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        [Route("api/category/count")]
        [ResponseType(typeof(List<Category>))]
        public async Task<IHttpActionResult> GetCategoryWithCount()
        {
            var categoryList = await _categoryService.GetCategoryWithCount();
            return Ok(categoryList);
        }

        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> PostCategory(Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _categoryService.Add(category);
            await _categoryService.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi", new { id = category.Id }, category);
        }


        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> Put(int id, Category category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != category.Id)
            {
                return BadRequest();
            }

            var dbCategory = await _categoryService.GetAsync(category.Id);

            if (dbCategory == null)
            {
                return NotFound();
            }

            dbCategory.CategoryName = category.CategoryName;
            dbCategory.Color = category.Color;
            await _categoryService.SaveChangesAsync();
            return Ok(category);
        }

        [ResponseType(typeof(Category))]
        public async Task<IHttpActionResult> Delete(int id)
        {
            Category category = await _categoryService.GetAsync(id);
            if (category == null)
            {
                return NotFound();
            }
            _categoryService.Delete(category);
            await _categoryService.SaveChangesAsync();
            return Ok(category);
        }

    }
}
