﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using AutoMapper;
using BusinessLayer;
using BusinessLayer.ConvertService;
using BusinessLayer.Dto;
using DatabaseLayer;
using DatabaseLayer.Entities;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    public class StockController : ApiController
    {
        private readonly IStockService _stockService;

        public StockController(IStockService stockService)
        {
            _stockService = stockService;
        }

        [Route("api/stock/category")]
        [ResponseType(typeof(List<Stock>))]
        public async Task<IHttpActionResult> GetListWithCategory()
        {
            var list = await _stockService.GetListWithCategory();
            var listDto = CovertStockToDto.ConvertToDto(list);
            return Ok(listDto);
        }

        public async Task<IHttpActionResult> GetCustomer(int id)
        {
            Stock stock = await _stockService.GetAsync(id);
            if (stock == null)
            {
                return NotFound();
            }

            return Ok(stock);
        }


    }
}
