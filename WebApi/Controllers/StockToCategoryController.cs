﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using BusinessLayer;
using BusinessLayer.Dto;

using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    public class StockToCategoryController : ApiController
    {
        private readonly IStockToCategoryService _stockToCategoryService;

        public StockToCategoryController(IStockToCategoryService stockToCategoryService)
        {
            _stockToCategoryService = stockToCategoryService;
        }

        [ResponseType(typeof(StockToCategoryDto))]
        public async Task<IHttpActionResult> Post(StockToCategoryDto stockToCategory)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _stockToCategoryService.AddLink(stockToCategory);
            await _stockToCategoryService.SaveChangesAsync();
            return StatusCode(HttpStatusCode.NoContent);
        }

        [ResponseType(typeof(StockToCategoryDto))]
        public async Task<IHttpActionResult> Delete(StockToCategoryDto stockToCategory)
        {
            _stockToCategoryService.DeleteLink(stockToCategory);
            await _stockToCategoryService.SaveChangesAsync();
            return Ok(stockToCategory);
        }

    }
}
