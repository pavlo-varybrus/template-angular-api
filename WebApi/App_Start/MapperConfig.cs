﻿using System.Reflection;
using System.Web.Http;
using Autofac;
using Autofac.Integration.WebApi;
using AutoMapper;
using DatabaseLayer.Entities;
using BusinessLayer.Dto;
using WebApi.Services;

namespace WebApi
{
    public class MapperConfig
    {
        public static void Register()
        {
            //var config = new MapperConfiguration(cfg =>
            //{
            //    cfg.CreateMap<Stock, StockDto>();
            //    cfg.CreateMap<Category, CategoryDto>().ForMember(dest => dest.Count, opt => opt.Ignore());
            //});
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<Stock, StockDto>();
                //.ForMember(dest => dest.Categories, opt => opt.MapFrom(src => src.Categories))
                //.PreserveReferences();
                cfg.CreateMap<Category, CategoryDto>()
                    .ForMember(dest => dest.Count, opt => opt.Ignore());
                //.ForMember(dest => dest.Stocks, opt => opt.MapFrom(src => src.Stocks))
                // .PreserveReferences();
            });
            Mapper.Configuration.AssertConfigurationIsValid();



            //        Mapper.CreateMap<Module, ModuleUI>()
            //.ForMember(s => s.Text, c => c.MapFrom(m => m.Name))
            //.ForMember(s => s.ImagePath, c => c.MapFrom(m => m.ImageName))
            //.ForMember(s => s.PageUIs, c => c.MapFrom(m => m.Pages));
            //        Mapper.CreateMap<Page, PageUI>();
            // Mapper.CreateMap<FacebookUser, ProspectModel>();

            // Mapper.Initialize(cfg => cfg.CreateMap<StockDto, Stock>());
            //.ForMember(dest => dest.ByUserId, opt => opt.MapFrom(src => src.ByUser.Id))
            //.ForMember(dest => dest.Categories, opt => opt.MapFrom(src => src.Categories)));

            // Mapper.Initialize(cfg => cfg.CreateMap<Category, CategoryDto>());
        }
    }
}