﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DatabaseLayer.Entities;

namespace BusinessLayer
{
    public interface IStockService : IGenericEntityOperation<Stock>
    {
        Task<List<Stock>> GetListWithCategory();
    }
}