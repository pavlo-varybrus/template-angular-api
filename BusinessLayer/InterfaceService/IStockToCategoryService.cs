﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLayer.Dto;
using DatabaseLayer.Entities;

namespace BusinessLayer
{
    public interface IStockToCategoryService : IGenericEntityOperation<Stock>
    {
        void AddLink(StockToCategoryDto stockToCategory);
        void DeleteLink(StockToCategoryDto stockToCategory);
    }
}