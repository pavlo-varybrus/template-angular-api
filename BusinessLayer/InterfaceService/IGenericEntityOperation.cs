﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public interface IGenericEntityOperation<TEntity> where TEntity : class
    {
        Task<TEntity> GetAsync(object id);
        Task<List<TEntity>> GetAsync();
        void Add(TEntity entity);
        void Delete(TEntity entity);
        Task<int> SaveChangesAsync();
    }
}