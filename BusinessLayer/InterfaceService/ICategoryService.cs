﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BusinessLayer.Dto;
using DatabaseLayer;
using DatabaseLayer.Entities;

namespace BusinessLayer
{
    public interface ICategoryService : IGenericEntityOperation<Category>
    {
        Task<List<CategoryDto>> GetCategoryWithCount();
        void SaveChanges();
    }
}