﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using DatabaseLayer;

namespace BusinessLayer
{
    public interface IGenericEntityOperation
    {
    }

    public class GenericEntityOperation<TEntity> : IGenericEntityOperation<TEntity>, IGenericEntityOperation where TEntity : class
    {
        protected readonly MainContext _context;
        private DbSet<TEntity> _dbSet;

        public GenericEntityOperation(MainContext context)
        {
            _context = context;
            this._dbSet = context.Set<TEntity>();
        }

        public async Task<int> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void SaveChanges()
        {
            _context.SaveChanges();
        }

        public Task<TEntity> GetAsync(object id)
        {
            return _dbSet.FindAsync(id);
        }

        public Task<List<TEntity>> GetAsync()
        {
            return _dbSet.ToListAsync();
        }

        public void Add(TEntity entity)
        {
            _dbSet.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
        }
    }
}