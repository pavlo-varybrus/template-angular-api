﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Dto;
using DatabaseLayer.Entities;

namespace BusinessLayer.ConvertService
{
    public class CovertStockToDto
    {
        public static List<StockDto> ConvertToDto(List<Stock> stock)
        {
            return stock.Select(x => ConvertToDto(x)).ToList();
        }
        public static StockDto ConvertToDto(Stock stock)
        {
            StockDto stockDto = AutoMapper.Mapper.Map<StockDto>(stock);
            foreach (var categoryDto in stockDto.Categories)
            {
                categoryDto.Stocks = null;
            }
            return stockDto;
        }
    }
}
