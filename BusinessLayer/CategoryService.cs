﻿using DatabaseLayer.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer.Dto;
using DatabaseLayer;

namespace BusinessLayer
{
    public class CategoryService : GenericEntityOperation<Category>, ICategoryService
    {
        public async Task<List<CategoryDto>> GetCategoryWithCount()
        {
            string sql = @"
select  c.[Id]
      ,c.[CategoryName]
      ,c.[CategoryType]
      ,c.[Color]
      ,c.[LastUpdate]
      ,c.[ByUser_Id],
	   count(*) as 'Count'
from dbo.Categories c inner
join dbo.StockCategories sc on c.Id = sc.Category_Id
group by c.[Id]
      ,c.[CategoryName]
      ,c.[CategoryType]
      ,c.[Color]
      ,c.[LastUpdate]
      ,c.[ByUser_Id]";
            return await _context.Database.SqlQuery<CategoryDto>(sql).ToListAsync();
        }

        public CategoryService(MainContext context) : base(context)
        {
        }
    }
}