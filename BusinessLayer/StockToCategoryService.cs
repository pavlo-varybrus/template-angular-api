﻿using DatabaseLayer.Entities;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Threading.Tasks;
using AutoMapper;
using BusinessLayer.Dto;
using DatabaseLayer;

namespace BusinessLayer
{
    public class StockToCategoryService : GenericEntityOperation<Stock>, IStockToCategoryService
    {
        public StockToCategoryService(MainContext context) : base(context)
        {

        }

        public async void AddLink(StockToCategoryDto stockToCategory)
        {
            Stock stock = await GetAsync(stockToCategory.StockId);
            Category category = await _context.Categories.FindAsync(stockToCategory.CategoryId);
            stock.Categories.Add(category);
        }

        public async void DeleteLink(StockToCategoryDto stockToCategory)
        {
            Stock stock = await GetAsync(stockToCategory.StockId);
            Category category = await _context.Categories.FindAsync(stockToCategory.CategoryId);
            stock.Categories.Remove(category);
        }
    }
}