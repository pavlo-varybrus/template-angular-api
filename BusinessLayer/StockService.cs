﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using DatabaseLayer;
using DatabaseLayer.Entities;

namespace BusinessLayer
{
    public class StockService : GenericEntityOperation<Stock>, IStockService
    {
        public async Task<List<Stock>> GetListWithCategory()
        {
            return await _context.Stocks.Include(x => x.Categories).ToListAsync();
        }

        public StockService(MainContext context) : base(context)
        {
        }
    }


}