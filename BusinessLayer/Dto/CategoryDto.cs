﻿using System;
using System.Collections.Generic;
using DatabaseLayer.Entities;

namespace BusinessLayer.Dto
{
    public class CategoryDto
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string CategoryType { get; set; }
        public string Color { get; set; }
        public ICollection<Stock> Stocks { get; set; }
        public ApplicationUser ByUser { get; set; }
        public DateTime LastUpdate { get; set; }

        public int Count { get; set; }
    }
}