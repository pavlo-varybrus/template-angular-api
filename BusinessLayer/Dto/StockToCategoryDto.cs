﻿namespace BusinessLayer.Dto
{
    public class StockToCategoryDto
    {
        public int StockId { get; set; }
        public int CategoryId { get; set; }

    }
}