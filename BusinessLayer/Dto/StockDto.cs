﻿using System;
using System.Collections.Generic;
using DatabaseLayer.Entities;
using DatabaseLayer.EntityEnum;

namespace BusinessLayer.Dto
{
    public class StockDto
    {
        public int Id { get; set; }
        public IEnumerable<CategoryDto> Categories { get; set; }
        public int Value { get; set; }
        public StockStatus Status { get; set; }
        public string ByUserId { get; set; }
        public DateTime LastUpdate { get; set; }
    }
}